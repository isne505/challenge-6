#ifndef FRACTION_H
#define FRACTION_H
class fraction
{
public:
	//constructors
	fraction();
		fraction(int N);
		fraction(int N,int D);
		//mutators
		void setNum(int);//the function that set new numenator
		void setDenom(int);//the function that set new denomenator
		//accessors
		int getNum();//the function that keep numenator
		int getDenom();//the function that keep denomenator

		fraction operator + (const fraction& f2);//the function that sum 2 fraction
		fraction operator - (const fraction& f2);//the function that subtract 2 fraction
		fraction operator * (const fraction& f2);//the function that multiply 2 fraction
		fraction operator / (const fraction& f2);//the function that divide 2 fraction
		fraction& operator ++ ();//the function that post increament for each fraction
		fraction& operator ++ (int a);//the function that pre increament for each fraction
		fraction& operator -- ();//the function that post decreament for each fraction
		fraction& operator -- (int a);//the function that pre decreament for each fraction
		bool operator == (const fraction& f2);//the function compare 2 fraction when it equal 
		bool operator != (const fraction& f2);//the function compare 2 fraction when it not equal 
		bool operator < (const fraction& f2);//the function compare 2 fraction when fraction1 less than fraction2 
		bool operator <= (const fraction& f2);
		bool operator > (const fraction& f2); //the function compare 2 fraction when fraction1 more than fraction2
		bool operator >= (const fraction& f2);
private: 
	int num;
	int denom;	
};
#endif // FRACTION_H