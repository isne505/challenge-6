#include "fraction.h"

fraction::fraction()
{
	num = 0;
	denom = 1;
}
fraction::fraction(int N)
{
	num = N;
	denom = 1;
}
fraction::fraction(int N,int D)
{
	num = N;
	denom = D;
}

void fraction::setNum(int n)
{
	num = n;
}

void fraction::setDenom(int d)
{
	denom = d;
}

int fraction::getNum()
{
	return num;
}
int fraction::getDenom()
{
	return denom;
}
fraction fraction::operator + (const fraction& f2)//when found + in source.cpp and got fraction to solve
{
	fraction temp(num * f2.denom + f2.num * denom,denom * f2.denom);
	return temp;
}
fraction fraction::operator - (const fraction& f2)//when found - in source.cpp and got fraction to solve
{
	fraction temp(num * f2.denom - f2.num * denom,denom * f2.denom);
	return temp;
}
fraction fraction::operator * (const fraction& f2)//when found * in source.cpp and got fraction to solve
{
	fraction temp(num * f2.num,denom * f2.denom);
	return temp;
}
fraction fraction::operator / (const fraction& f2)//when found / in source.cpp and got fraction to solve
{
	fraction temp(num * f2.denom,denom * f2.num);
	return temp;
}
fraction& fraction::operator ++ () //take fraction to post increse numenator by denomenator 
{
	num += denom;
	return *this;
}
fraction& fraction::operator ++ (int a) //take fraction to pre increse numenator by denomenator 
{
	num += denom;
	return *this;
}
fraction& fraction::operator -- () //take fraction to post decrese numenator by denomenator 
{
	num -= denom;
	return *this;
}
fraction& fraction::operator -- (int a) //take fraction to pre decrese numenator by denomenator 
{
	num -= denom;
	return *this;
}
bool fraction::operator == (const fraction& f2) //take fraction1 and fraction2 and compare it
{
	if (num * f2.denom == denom * f2.num) //if fraction1 equal fraction2 return true
	{
		return true;
	}
	else
	{
		return false;
	}
}
bool fraction::operator != (const fraction& f2)//take fraction1 and fraction2 and compare it
{
	if (num * f2.denom != denom * f2.num)//if fraction1 not equal fraction2 return true
	{
		return true;
	}
	else
	{
		return false;
	}
}
bool fraction::operator < (const fraction& f2)//take fraction1 and fraction2 and compare it
{
	if (num * f2.denom < denom * f2.num)//if fraction1 less than fraction2 return true
	{
		return true;
	}
	else
	{
		return false;
	}
}
bool fraction::operator <= (const fraction& f2)//take fraction1 and fraction2 and compare it
{
	if (num * f2.denom <= denom * f2.num)//if fraction1 less than or equal fraction2 return true
	{
		return true;
	}
	else
	{
		return false;
	}
}
bool fraction::operator > (const fraction& f2)//take fraction1 and fraction2 and compare it
{
	if (num * f2.denom > denom * f2.num)//if fraction1 more than fraction2 return true
	{
		return true;
	}
	else
	{
		return false;
	}
}bool fraction::operator >= (const fraction& f2)//take fraction1 and fraction2 and compare it
{
	if (num * f2.denom >= denom * f2.num)//if fraction1 more than or equal fraction2 return true

	{
		return true;
	}
	else
	{
		return false;
	}
}